## Simple Grunt Setup

This is a simple grunt setup which has been support scss to css minified and multiple js to a chunk of js.
How to use :

- install required files
```
npm install
```
- run grunt file 
```
grunt
```
- edit files in folder `src`